﻿using System;
using System.Collections.Generic;
using System.Text;

namespace B19_Ex02_1
{
    public class ArrayOperations<T>
    {
        public static string Join(string i_Splitter, T[] i_Values)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < i_Values.Length; i++)
            {
                stringBuilder.Append(" " + i_Values[i]);
            }

            return stringBuilder + " ";
        }
    }
}
