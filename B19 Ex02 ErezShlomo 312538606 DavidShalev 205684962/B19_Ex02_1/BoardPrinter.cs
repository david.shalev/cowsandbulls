﻿using System;
using System.Collections.Generic;
using System.Text;
using Ex02.ConsoleUtils;


namespace B19_Ex02_1
{
    public static class BoardPrinter
    {
        private const int k_LeftCellWidth = 9;
        private const int k_RightCellWidth = 7;
        public static void DrawBoard(int i_NumberOfGuesses, List<Guess> i_GuessesList, char[] i_HiddenLetters, bool i_ShowHiddenLetters)
        {
            Screen.Clear();
            printHeaders(i_HiddenLetters, i_ShowHiddenLetters);
            printGuesses(i_NumberOfGuesses, i_GuessesList);
            Console.WriteLine();
        }

        private static void printHeaders(char[] i_HiddenLetters, bool i_ShowHiddenLetters)
        {
            Console.WriteLine("Current board status:" + Environment.NewLine);
            drawLine("Pins:", "Result:");
            drawLine(i_ShowHiddenLetters ? getHiddenLettersString(i_HiddenLetters) : " # # # # ", "");
        }

        private static void printGuesses(int i_NumberOfGuesses, List<Guess> i_GuessesList)
        {
            foreach (Guess guess in i_GuessesList)
            {
                drawLine(guess.GetPinsString(), guess.GetResultString());
            }

            for (int i = 0; i < i_NumberOfGuesses - i_GuessesList.Count; i++)
            {
                drawEmptyLine();
            }
        }

        private static void drawEmptyLine()
        {
            drawLine("", "");
        }

        private static void drawLine(string i_LeftCell, string i_RightCell)
        {
            int leftCellLength = i_LeftCell.Length;
            int rightCellLength = i_RightCell.Length;

            for (int i = 0; i < k_LeftCellWidth - leftCellLength; i++)
            {
                i_LeftCell += ' ';
            }

            for (int i = 0; i < k_RightCellWidth - rightCellLength; i++)
            {
                i_RightCell += ' ';
            }

            Console.WriteLine("|{0}|{1}|", i_LeftCell, i_RightCell);
            drawSplitter();
        }

        private static void drawSplitter()
        {
            Console.WriteLine("|{0}|{1}|", "=========", "=======");
        }

        private static string getHiddenLettersString(char[] i_HiddenLetters)
        {
            return ArrayOperations<char>.Join(" ", i_HiddenLetters);
        }
    }
}
