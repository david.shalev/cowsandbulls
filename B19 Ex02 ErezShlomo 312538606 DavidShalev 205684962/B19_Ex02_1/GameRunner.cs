﻿using System;
using System.Collections.Generic;
using System.Text;
using B19_Ex02_1.Results;

namespace B19_Ex02_1
{
    public class GameRunner
    {
        private const int k_NumberOfHiddenLetters = 4;
        private readonly GameUserCommunicator r_GameUserCommunicator;
        private List<Guess> m_GuessesList;
        private int m_NumberOfGuesses;
        private char[] m_HiddenLetters;

        public GameRunner()
        {
            r_GameUserCommunicator = new GameUserCommunicator();
        }

        public void Start()
        {
            bool isExitRequested = false;

            while (!isExitRequested)
            {
                eTurnResult lastTurnResult;
                generateHiddenLetters();
                m_NumberOfGuesses = r_GameUserCommunicator.GetNumberOfTotalGuesses();
                m_GuessesList = new List<Guess>();
                bool showHiddenLetters = false;
                BoardPrinter.DrawBoard(m_NumberOfGuesses, m_GuessesList, m_HiddenLetters, showHiddenLetters);
                do
                {
                    lastTurnResult = getNextGuessResult();
                    showHiddenLetters = isGameFinished(lastTurnResult);
                    BoardPrinter.DrawBoard(m_NumberOfGuesses, m_GuessesList, m_HiddenLetters, showHiddenLetters);
                }
                while (m_GuessesList.Count < m_NumberOfGuesses && lastTurnResult == eTurnResult.BadGuess);

                r_GameUserCommunicator.PrintFinishedGameMessage(lastTurnResult, m_GuessesList.Count);
                isExitRequested = lastTurnResult == eTurnResult.Exit || !r_GameUserCommunicator.CheckIfUserWantsToStartNewGame();
            }
        }

        private bool isGameFinished(eTurnResult i_LastTurnResult)
        {
            return m_GuessesList.Count == m_NumberOfGuesses || i_LastTurnResult == eTurnResult.BullsEye;
        }

        private void generateHiddenLetters()
        {
            List<char> availableCharacters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
            m_HiddenLetters = new char[k_NumberOfHiddenLetters];
            Random randomGenerator = new Random();

            for (int i = 0; i < m_HiddenLetters.Length; i++)
            {
                int generatedIndex = randomGenerator.Next(0, availableCharacters.Count);
                m_HiddenLetters[i] = availableCharacters[generatedIndex];
                availableCharacters.RemoveAt(generatedIndex);
            }
        }

        private eTurnResult getNextGuessResult()
        {
            eTurnResult turnResult = eTurnResult.BadGuess;
            GuessInputResult inputResult = r_GameUserCommunicator.GetGuessFromUser(k_NumberOfHiddenLetters);

            if (inputResult.ExitRequested)
            {
                turnResult = eTurnResult.Exit;
            }
            else
            {
                inputResult.Guess.UpdateGuessResult(m_HiddenLetters);
                m_GuessesList.Add(inputResult.Guess);
                if (inputResult.Guess.NumberOfBullsEye == m_HiddenLetters.Length)
                {
                    turnResult = eTurnResult.BullsEye;
                }
            }

            return turnResult;
        }
    }
}
