﻿using System;
using System.Collections.Generic;
using System.Text;
using B19_Ex02_1.Results;

namespace B19_Ex02_1
{
    public class GameUserCommunicator
    {
        const int k_MaxNumberOfGuesses = 10;
        const int k_MinNumberOfGuesses = 4;

        public GuessInputResult GetGuessFromUser(int i_NumberOfHiddenLetters)
        {
            Console.WriteLine("Please type your next guess <A B C D> or 'Q' to quit");
            GuessInputResult guessInputResult = new GuessInputResult();
            string input = Console.ReadLine();
            guessInputResult.ExitRequested = (input == "Q" || input == "q");

            if (!guessInputResult.ExitRequested)
            {
                GuessParseResult parseResult = Guess.TryParse(input, i_NumberOfHiddenLetters, out Guess inputGuess);
                while (!parseResult.Succeeded && !guessInputResult.ExitRequested)
                {
                    Console.WriteLine(parseResult.Error);
                    input = Console.ReadLine();
                    guessInputResult.ExitRequested = (input == "Q" || input == "q");
                    if (!guessInputResult.ExitRequested)
                    {
                        parseResult = Guess.TryParse(input, i_NumberOfHiddenLetters, out inputGuess);
                    }
                }

                guessInputResult.Guess = inputGuess;
            }

            return guessInputResult;
        }

        public bool CheckIfUserWantsToStartNewGame()
        {
            return UserInput.GetYesOrNoFromUser("Would you like to start a new game? <Y/N>");
        }

        public void PrintFinishedGameMessage(eTurnResult i_LastTurnResult, int i_NumberOfGuesses)
        {
            switch (i_LastTurnResult)
            {
                case eTurnResult.Exit:
                    Console.WriteLine("Goodbye!");
                    break;
                case eTurnResult.BullsEye:
                    Console.WriteLine("You guessed after {0} steps!", i_NumberOfGuesses);
                    break;
                case eTurnResult.BadGuess:
                    Console.WriteLine("No more guesses allowed. You Lost.");
                    break;
            }
        }

        public int GetNumberOfTotalGuesses()
        {
            Console.WriteLine("Please enter number of guesses:");
            int numOfTotalGuesses = UserInput.GetIntFromUser();

            while (!isValidNumberOfGuessesInput(numOfTotalGuesses))
            {
                Console.WriteLine("The input number must be between {0} and {1}", k_MinNumberOfGuesses, k_MaxNumberOfGuesses);
                numOfTotalGuesses = UserInput.GetIntFromUser();
            }

            return numOfTotalGuesses;
        }

        private static bool isValidNumberOfGuessesInput(int i_Input)
        {
            return i_Input <= k_MaxNumberOfGuesses && i_Input >= k_MinNumberOfGuesses;
        }
    }
}
