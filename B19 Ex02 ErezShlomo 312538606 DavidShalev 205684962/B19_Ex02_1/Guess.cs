﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using B19_Ex02_1.Results;

namespace B19_Ex02_1
{
    public class Guess
    {
        public int NumberOfBullsEye { get; private set; }
        public int NumberOfTotalMiss { get; private set; }
        public int NumberOfCorrectLetterOnly { get; private set; }
        public char[] LettersGuessed { private get; set; }

        public string GetPinsString()
        {
            return ArrayOperations<char>.Join(" ", LettersGuessed);
        }

        public string GetResultString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < NumberOfBullsEye; i++)
            {
                stringBuilder.Append("V ");
            }

            for (int i = 0; i < NumberOfCorrectLetterOnly; i++)
            {
                stringBuilder.Append("X ");
            }

            if (stringBuilder.Length > 0)
            {
                stringBuilder.Remove(stringBuilder.Length - 1, 1);
            }
            return stringBuilder.ToString();
        }

        public void UpdateGuessResult(char[] i_HiddenLetters)
        {
            NumberOfBullsEye = 0;
            NumberOfTotalMiss = 0;
            NumberOfCorrectLetterOnly = 0;

            for (int i = 0; i < i_HiddenLetters.Length; i++)
            {
                if (LettersGuessed[i] == i_HiddenLetters[i])
                {
                    NumberOfBullsEye++;
                }
                else if (Array.IndexOf(i_HiddenLetters, LettersGuessed[i]) != -1)
                {
                    NumberOfCorrectLetterOnly++;
                }
                else
                {
                    NumberOfTotalMiss++;
                }
            }
        }

        public static GuessParseResult TryParse(string i_UserInputGuessString, int i_NumberOfLetters, out Guess io_Guess)
        {
            io_Guess = new Guess();
            GuessParseResult parseResult = validateGuessStringToParse(i_UserInputGuessString, i_NumberOfLetters);

            if (parseResult.Succeeded)
            {
                io_Guess.LettersGuessed = new char[i_NumberOfLetters];
                int guessLettersIndex = 0;
                for (int i = 0; i < i_UserInputGuessString.Length; i += 2)
                {
                    io_Guess.LettersGuessed[guessLettersIndex++] = i_UserInputGuessString[i];
                }
            }

            return parseResult;
        }

        private static GuessParseResult validateGuessStringToParse(string i_GuessStringToParse, int i_NumberOfLetters)
        {
            GuessParseResult validateResult = new GuessParseResult { Succeeded = true };

            if (i_GuessStringToParse.Length != (i_NumberOfLetters * 2) - 1)
            {
                validateResult.SetError("The string is invalid, please enter a string like this: <A B C D>");
            }
            else
            {
                List<char> guessedLetters = new List<char>();
                for (int i = 0; i < i_GuessStringToParse.Length; i++)
                {
                    if (i % 2 == 1)
                    {
                        if (i_GuessStringToParse[i] != ' ')
                        {
                            validateResult.SetError("Please enter spaces between characters");
                        }
                        continue;
                    }
                    if (i_GuessStringToParse[i] < 'A' || i_GuessStringToParse[i] > 'Z')
                    {
                        validateResult.SetError("Please enter 4 Capital letters.");
                    }
                    else if (i_GuessStringToParse[i] > 'H')
                    {
                        validateResult.SetError("The letter '" + i_GuessStringToParse[i] + "' is out of bounds (A-H)");
                    }

                    if (guessedLetters.Contains(i_GuessStringToParse[i]))
                    {
                        validateResult.SetError("The letter " + i_GuessStringToParse[i] + " selected more than one time");
                    }

                    guessedLetters.Add(i_GuessStringToParse[i]);
                }
            }

            return validateResult;
        }
    }
}
