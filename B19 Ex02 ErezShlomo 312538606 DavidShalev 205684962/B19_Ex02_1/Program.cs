﻿using System;
using System.Collections.Generic;
using System.Text;

namespace B19_Ex02_1
{
    public class Program
    {
        public static void Main(string[] i_Arguments)
        {
            GameRunner runner = new GameRunner();
            runner.Start();
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
