﻿using System;
using System.Collections.Generic;
using System.Text;

namespace B19_Ex02_1.Results
{
    public class GuessInputResult
    {
        public Guess Guess { get; set; }

        public bool ExitRequested { get; set; }
    }
}
