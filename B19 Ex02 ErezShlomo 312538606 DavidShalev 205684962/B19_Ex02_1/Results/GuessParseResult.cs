﻿using System;
using System.Collections.Generic;
using System.Text;

namespace B19_Ex02_1.Results
{
    public class GuessParseResult
    {
        public bool Succeeded { get; set; }
        public string Error { get; set; }

        public void SetError(string i_Error)
        {
            Error = i_Error;
            Succeeded = false;
        }
    }
}
