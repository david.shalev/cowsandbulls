﻿using System;
using System.Collections.Generic;
using System.Text;

namespace B19_Ex02_1.Results
{
    public enum eTurnResult
    {
        None,
        BadGuess,
        BullsEye,
        Exit
    }
}
