﻿using System;

namespace B19_Ex02_1
{
    public static class UserInput
    {
        public static int GetIntFromUser()
        {
            bool isNumber = int.TryParse(Console.ReadLine(), out int input);
            while (!isNumber)
            {
                Console.WriteLine("The input must be a number.");
                isNumber = int.TryParse(Console.ReadLine(), out input);
            }

            return input;
        }

        public static bool GetYesOrNoFromUser(string i_Question)
        {
            Console.WriteLine(i_Question);
            string input = Console.ReadLine();

            while (input != "Y" && input != "N" && input != "y" && input != "n")
            {
                Console.WriteLine("Please enter a valid input. (Y or N)");
                input = Console.ReadLine();
            }

            return (input == "Y" || input == "y");
        }
    }
}
