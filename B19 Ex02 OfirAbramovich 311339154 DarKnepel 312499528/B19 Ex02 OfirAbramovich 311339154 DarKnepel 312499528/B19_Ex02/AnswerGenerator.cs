﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace B19_Ex02
{
    public class AnswerGenerator
    {
        private Dictionary<char, int> m_Dictionary;

        public AnswerGenerator()
        {
            m_Dictionary = generateAnswer();
        }

        public Dictionary<char, int> Dict
        {
            get
            {
                return m_Dictionary;
            }
        }

        // $G$ NTT-007 (-10) There's no need to re-instantiate the Random instance each time it is used.
        private Dictionary<char, int> generateAnswer()
        {
            Random rnd = new Random();
            char randomChar;

            m_Dictionary = new Dictionary<char, int>();
            while (m_Dictionary.Count < 4)
            {
                rnd = new Random();
                randomChar = (char)rnd.Next('A', 'I');
                if (!m_Dictionary.Keys.Contains(randomChar))
                {
                    m_Dictionary.Add(randomChar, m_Dictionary.Count);
                }
            }

            return m_Dictionary;
        }
    }
}
