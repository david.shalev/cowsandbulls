﻿using System.Collections.Generic;

namespace B19_Ex02
{
    public class Board
    {
        private string[] m_BoardMatrix;

        public Board (int i_NumberOfGuesses)
        {
            m_BoardMatrix = IOHandler.CreateConsoleBoard(i_NumberOfGuesses);
        }

        public void Show()
        {
            IOHandler.ClearConsole();
            IOHandler.PrintGameBoard(m_BoardMatrix);
        }

        public void InsertDataToBoard(int i_GuessNumber, char[] i_Guess, List<char> i_Result)
        {
            IOHandler.InsertDataToBoard(m_BoardMatrix, i_GuessNumber, i_Guess, i_Result);
        }

        public void RevealAnswer(string i_Answer)
        {
            IOHandler.RevealAnswer(m_BoardMatrix, i_Answer);
            Show();
        }
    }

   
}
