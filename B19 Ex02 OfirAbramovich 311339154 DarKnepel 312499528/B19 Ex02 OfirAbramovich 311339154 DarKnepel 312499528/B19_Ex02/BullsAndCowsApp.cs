﻿using System;
using System.Collections.Generic;

// $G$ DSN-999 (-5) class and file name should be called Program 
// $G$ DSN-999 (-5) this class should be the entry point for the program and not contain any parts of the application



namespace B19_Ex02
{
    public class BullsAndCowsApp
    {
        public const int k_AnswerSize = 4;
        public const char k_FirstLetterInRange = 'A';
        public const char k_LastLetterInRange = 'H';
        public const int k_MinimumGuesses = 4;
        public const int k_MaximumGuesses = 10;

        public static void Main()
        {
            play();
        }

        private static void play()
        {
            int numberOfGuesses;
            GameRound round;
            char[] inputGuess;

            numberOfGuesses = getNumberOfGuesses();
            AnswerGenerator answer = new AnswerGenerator();
            Board gameBoard = new Board(numberOfGuesses);
            gameBoard.Show();

            for (int i = 0; i < numberOfGuesses; i++)
            {
                inputGuess = getGuessFromUser();
                round = new GameRound(inputGuess, answer);
                gameBoard.InsertDataToBoard(i, round.Guess, round.Result);
                gameBoard.Show();
                if (round.IsCorrectGuess())
                {
                    IOHandler.PrintWonOrLostMsg(true, i + 1);
                    askToPlayAgain();

                    return;
                }
            }

            gameBoard.RevealAnswer(answer.ToString());
            IOHandler.PrintWonOrLostMsg(false);
            askToPlayAgain();
        }

        private static int getNumberOfGuesses()
        {
            int numberOfGuesses;

            IOHandler.AskForInputFromUser(1);
            while (true)
            {
                if (!int.TryParse(IOHandler.GetInputFromUser(), out numberOfGuesses))
                {
                    IOHandler.PrintErr(1);
                    continue;
                }
                else if (numberOfGuesses < k_MinimumGuesses || numberOfGuesses > k_MaximumGuesses)
                {
                    IOHandler.PrintErr(2);
                    continue;
                }

                return numberOfGuesses;
            }
        }

        // $G$ CSS-999 (-3) You should have used constants here.
        private static char[] getGuessFromUser()
        {
            string inputGuess;

            IOHandler.AskForInputFromUser(2);
            while (true)
            {
                inputGuess = IOHandler.GetInputFromUser();
                if (inputGuess.Equals(""))
                {
                    /// if input is enpty
                    IOHandler.PrintErr(3);
                }
                else if (inputGuess.Equals("Q"))
                {
                    exit();
                }
                else if (!isSpaceSeperated(inputGuess))
                {
                    /// if guess letters are not space separated
                    IOHandler.PrintErr(4);
                }
                else if (inputGuess.Length != 2 * k_AnswerSize - 1)
                {
                    // if input is not the right length
                    IOHandler.PrintErr(5);
                }
                else if (isDuplicated(inputGuess))
                {
                    /// if there exist duplicated letters
                    IOHandler.PrintErr(6);
                }
                else if (!isInRange(inputGuess))
                {
                    /// if input letter are not in the correct range
                    IOHandler.PrintErr(7);
                }
                else
                {
                    return inputGuess.Replace(" ", string.Empty).ToCharArray(); /// converts guess into a char array if input is valid
                }
            }
        }

        private static bool isSpaceSeperated(string i_GuessInput)
        {
            bool isSpaced = true;

            for (int i = 1; i < i_GuessInput.Length; i += 2)
            {
                if (!i_GuessInput[i].Equals(' '))
                {
                    isSpaced = false;
                }
            }

            return isSpaced;
        }

        private static bool isDuplicated(string i_GuessInput)
        {
            HashSet<char> lettersInput = new HashSet<char>();

            for (int i = 0; i < i_GuessInput.Length; i += 2)
            {
                lettersInput.Add(i_GuessInput[i]);
            }

            return lettersInput.Count != 4;
        }

        private static bool isInRange(string i_GuessInput)
        {
            bool isRightRange = true;

            for (int i = 0; i < i_GuessInput.Length; i += 2)
            {
                if (i_GuessInput[i] < k_FirstLetterInRange || i_GuessInput[i] > k_LastLetterInRange)
                {
                    isRightRange = false;
                    break;
                }
            }

            return isRightRange;
        }

        private static void askToPlayAgain()
        {
            if (IOHandler.IsUserWillingToPlayAgain())
            {
                IOHandler.ClearConsole();
                play();
            }
            else
            {
                exit();
            }
        }

        private static void exit()
        {
            IOHandler.PrintGoodByeMsg();
            Environment.Exit(0);
        }
    }
}
