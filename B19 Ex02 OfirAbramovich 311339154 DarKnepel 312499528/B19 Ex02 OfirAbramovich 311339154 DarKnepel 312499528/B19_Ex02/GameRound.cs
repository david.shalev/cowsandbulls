﻿using System.Collections.Generic;
using System.Linq;

namespace B19_Ex02
{
    public class GameRound
    {
        private readonly char[] m_Guess;
        private readonly List<char> m_Result;
        private bool m_IsWinner;

        public GameRound(char[] i_Guess, AnswerGenerator i_Ans)
        {
            m_IsWinner = false;
            m_Guess = i_Guess;
            m_Result = getResult(i_Ans);
        }

        public char[] Guess
        {
            get
            {
                return m_Guess;
            }
        }
        public List<char> Result
        {
            get
            {
                return m_Result;
            }
        }

        private List<char> getResult(AnswerGenerator i_Ans)
        {
            List<char> result = new List<char>();
            for (int i = 0; i < m_Guess.Length; i++)
            {
                if (i_Ans.Dict.Keys.Contains(m_Guess[i]))
                {
                    if (i == i_Ans.Dict[m_Guess[i]])
                    {
                        result.Insert(0, 'V');
                    }
                    else
                    {
                        result.Add('X');
                    }
                }

                m_IsWinner = result.Count == BullsAndCowsApp.k_AnswerSize && !result.Contains('X') ? true : false;
            }

            return result;
        }

        public bool IsCorrectGuess()
        {
            return m_IsWinner;
        }
    }
}
