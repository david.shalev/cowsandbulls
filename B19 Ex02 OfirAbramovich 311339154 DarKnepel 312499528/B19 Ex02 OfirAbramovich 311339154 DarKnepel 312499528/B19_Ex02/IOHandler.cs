﻿using System;
using System.Collections.Generic;
using System.Text;
using Ex02.ConsoleUtils;

namespace B19_Ex02
{
    public class IOHandler
    {
        private const char k_ColSeperator = '¦';
        private const char k_RowSeperator = '=';
        private const string k_BlankGuess = "         ";
        private const string k_BlankResult = "       ";

        public static string GetInputFromUser()
        {
            return Console.ReadLine();
        }

        public static void AskForInputFromUser(int i_MsgStatus) 
        {
            switch (i_MsgStatus)
            {
                case 1:
                    Console.WriteLine(@"Please type number of guesses in range of {0}-{1}", BullsAndCowsApp.k_MinimumGuesses, BullsAndCowsApp.k_MaximumGuesses);
                    break;
                case 2:
                    Console.WriteLine(System.Environment.NewLine + "Please type your next guess <A B C D> or 'Q' to quit");
                    break;
            }
        }

        public static void PrintErr(int i_ErrNumber)
        {
            switch (i_ErrNumber)
            {
                case 1:
                    Console.WriteLine("Invalid input, please try again");
                    break;
                case 2:
                    Console.WriteLine("Number not in range, please try again");
                    break;
                case 3:
                    Console.WriteLine("No input inserted, please try again");
                    break;
                case 4:
                    Console.WriteLine("Wrong format of <A B C D>, please try again");
                    break;
                case 5:
                    Console.WriteLine("Input is not the right length, please try again");
                    break;
                case 6:
                    Console.WriteLine("Duplicats in input, please try again");
                    break;
                case 7:
                    Console.WriteLine("One or more letters are not in range A - H, please try again");
                    break;
            }
        }

        public static void PrintGoodByeMsg()
        {
            Console.WriteLine("Exitting program... Bye Bye");
        }

        public static bool IsUserWillingToPlayAgain()
        {
            string input = null;

            Console.WriteLine("Would you like to start a new game? (Y/N)");
            while (!isValidYesNo(input = Console.ReadLine()))
            {
                Console.WriteLine("Invalid inpu, please try again");
            }

            return input.Equals("Y");
        }

        // $G$ CSS-999 (-3) You should have used constants here.
        private static bool isValidYesNo(string i_Input)
        {
            return (i_Input.Equals("Y") || i_Input.Equals("N"));
        }

        public static void PrintWonOrLostMsg(bool i_isWinner, int i_RoundNumber = -1)
        {
            string stepOrSteps;

            if (i_isWinner)
            {
                stepOrSteps = i_RoundNumber == 1 ? "step" : "steps";
                Console.WriteLine(Environment.NewLine + @"You guessed after {0} {1}!", i_RoundNumber, stepOrSteps);
            }
            else
            {
                Console.WriteLine(System.Environment.NewLine + "No more guesses allowed. You Lost.");
            }
        }

        public static string[] CreateConsoleBoard(int i_NumberOfGuesses)
        {
            string[] consoleBoardMatrix = new string[4 + i_NumberOfGuesses * 2];
            for (int i = 0; i < consoleBoardMatrix.Length; i++)
            {
                if (i == 0)
                {
                    setRow(consoleBoardMatrix, i, "Pins:    ", "Result:");
                }
                else if (i == 2)
                { 
                    setRow(consoleBoardMatrix, i, " # # # # ");
                }
                else if (i % 2 != 0)
                {
                    setMarginRow(consoleBoardMatrix, i);
                }
                else
                {
                    setRow(consoleBoardMatrix, i);
                }
            }

            return consoleBoardMatrix;
        }

        // $G$ DSN-999 (-5) bad programming - to many variables in the function signature
        private static void setRow(string[] i_consoleBoardMatrix, int i_RowNumber, string i_LeftCell = k_BlankGuess, string i_RightCell = k_BlankResult)
        {
            i_consoleBoardMatrix[i_RowNumber] = string.Format("{0}{1,-9}{0}{2,-7}{0}", k_ColSeperator, i_LeftCell, i_RightCell);
        }

        public static void InsertDataToBoard(string[] i_consoleBoardMatrix, int i_GuessNumber, char[] i_Guess, List<char> i_Result)
        {
            int rowNumber = 4 + i_GuessNumber * 2;
            setRow(i_consoleBoardMatrix, rowNumber, ParseGuess(i_Guess), ParseResult(i_Result));
        }

        // $G$ CSS-013 (-3) Input parameters names should start with i_PascaleCase.
        private static void setMarginRow(string[] i_consoleBoardMatrix, int i_RowNumber)
        {
            string rowSeperatorLeft = new string(k_RowSeperator, 9);
            string rowSeperatorRight = new string(k_RowSeperator, 7);

            i_consoleBoardMatrix[i_RowNumber] = string.Format("{0}{1}{0}{2}{0}", k_ColSeperator, rowSeperatorLeft, rowSeperatorRight);
        }

        // $G$ CSS-013 (-3) Input parameters names should start with i_PascaleCase.
        public static void PrintGameBoard(string[] i_consoleBoardMatrix)
        {
            Console.WriteLine("Current board status:" + Environment.NewLine);
            for (int i = 0; i < i_consoleBoardMatrix.Length; i++)
            {
                Console.WriteLine(i_consoleBoardMatrix[i]);
            }
        }

        public static void RevealAnswer(string[] i_consoleBoardMatrix, string i_Answer)
        {
            setRow(i_consoleBoardMatrix, 2, i_Answer);
        }

        public static string ParseGuess(char[] i_Guess)
        {
            StringBuilder parsedGuess = new StringBuilder(" ");
            foreach (char guessChar in i_Guess)
            {
                parsedGuess.Append(guessChar + " ");
            }

            return parsedGuess.ToString();
        }

        public static string ParseResult(List<char> i_Result)
        {
            StringBuilder parsedGuess = new StringBuilder();
            foreach (char resultChar in i_Result)
            {
                parsedGuess.Append(resultChar + " ");
            }

            if (parsedGuess.Length > 0)
            {
                parsedGuess.Length--; /// trims the last redundent space
            }

            return parsedGuess.ToString();
        }

        public static void ClearConsole()
        {
            Screen.Clear();
        }
    }
}
