﻿using System;
using System.Collections.Generic;
using System.Text;
using Ex02.ConsoleUtils;

namespace bullsAndCows
{
    public static class boardPrinter
    {
        private const int leftCellLength = 9;
        private const int rightCellLength = 7;

        public static void drawBoard(int numOfGuesses, List<gameRound> pastGameRounds,
            char[] answerLetters, bool isGameOver)
        {
            Screen.Clear();
            printHeaders(answerLetters, isGameOver);
            printGuesses(numOfGuesses, pastGameRounds);
        }



        private static void printHeaders(char[] answerLetters, bool isGameOver)
        {
            Console.WriteLine("Current board status:" + Environment.NewLine);
            drawLine("Pins:", "Result:");
            if (!isGameOver) drawLine(" # # # #", "");
            else drawLine(invertToStringWithSpaces(answerLetters), "");
        }

        private static void printGuesses(int numOfGuesses, List<gameRound> pastGameRounds)
        {
            for (int i = 0; i < pastGameRounds.Count; i++)
            {
                string guessString = invertToStringWithSpaces(pastGameRounds[i].guess);
                string guessResult = invertToStringWithSpaces(pastGameRounds[i].guessResult);
                drawLine(guessString, guessResult);
            }

            for (int i = 0; i < numOfGuesses - pastGameRounds.Count; i++)
            {
                drawLine("", "");
            }
        }

        public static void drawLine(string leftString, string rightString)
        {
            int leftStringLength = leftString.Length;
            int rightStringLength = rightString.Length;

            for (int i = 0; i < leftCellLength - leftStringLength; i++)
            {
                leftString += " ";
            }

            for (int i = 0; i < rightCellLength - rightStringLength; i++)
            {
                rightString += " ";
            }

            Console.WriteLine("|{0}|{1}|", leftString, rightString);
            drawSplitter();
        }

        private static void drawSplitter()
        {
            Console.WriteLine("|{0}|{1}|", "=========", "=======");
        }

        private static string invertToStringWithSpaces(char[] letters)
        {
            string str = " ";
            for (int i = 0; i < letters.Length; i++)
            {
                str += letters[i] + " ";
            }

            return str;
        }

        private static string invertToStringWithSpaces(List<char> letters)
        {
            if (letters.Count == 0) return "";
            string str = letters[0].ToString();
            for (int i = 1; i < letters.Count; i++)
            {
                str += " " + letters[i];
            }

            return str;

        }
    }
}