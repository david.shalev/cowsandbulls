﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace bullsAndCows
{
    public class gameRound
    {
        public char[] guess { get;  }
        public List<char> guessResult { get;  }
        public bool isWinner { get; }

        public gameRound(char[] guess, char[] answerLetters)
        {
            this.isWinner = false;
            this.guess = guess;
            this.guessResult = getGuessResult(answerLetters);
            if (guessResult.Count == 4 && !guessResult.Contains('X'))
                this.isWinner = true;
        }

        private List<char> getGuessResult(char[] answerLetters)
        {
            List<char> guessResult = new List<char>();
            int numOfVs = 0;
            int numOfXs = 0;
            for (int i = 0; i < guess.Length; i++)
            {
                if (guess[i] == answerLetters[i]) numOfVs++;
                else if (answerLetters.Contains(guess[i])) numOfXs++;
            }

            for (int i = 0; i < numOfVs; i++)
            {
                guessResult.Add('V');
            }

            for (int i = 0; i < numOfXs; i++)
            {
                guessResult.Add('X');
            }

            return guessResult;
        }

        
    }
}

