﻿using System;
using System.Collections.Generic;

namespace bullsAndCows
{
    public class gameRunner
    {
        private const int minNumOfGuesses = 4;
        private const int maxNumOfGuesses = 10;
        private const int numOfAnswerLetters = 4;
        private const int numOfGuessLetters = 4;
        private readonly List<char> legalLetters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };


        public void start()
        {
            while (true)
            {
                int numOfGuesses = userCommunicator.getNumOfGuesses(minNumOfGuesses, maxNumOfGuesses);
                List<gameRound> gameRounds = new List<gameRound>();
                char[] answerLetters = getAnswerLetters();
                bool isGameOver = false;
                boardPrinter.drawBoard(numOfGuesses, gameRounds, answerLetters, isGameOver);
                while (gameRounds.Count < numOfGuesses)
                {
                    char[] userGuess = userCommunicator.getUserGuess(numOfGuessLetters, legalLetters);
                    gameRound gameRound = new gameRound(userGuess, answerLetters);
                    gameRounds.Add(gameRound);
                    boardPrinter.drawBoard(numOfGuesses, gameRounds, answerLetters, gameRound.isWinner);
                    if (gameRound.isWinner) break;
                    if (gameRounds.Count == numOfGuesses)
                    {
                        isGameOver = true;
                        boardPrinter.drawBoard(numOfGuesses, gameRounds, answerLetters, isGameOver);
                    }
                }

                if (!userCommunicator.isNewGame(isGameOver)) break;
            }
           
        }


        private  char[] getAnswerLetters()
        {
            List<char> availableLetters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
            char[] answerLetters = new char[numOfAnswerLetters];
            Random randomGenerator = new Random();
            for (int i = 0; i < numOfAnswerLetters; i++)
            {
                int randomNumber = randomGenerator.Next(0, availableLetters.Count);
                answerLetters[i] = availableLetters[randomNumber];
                availableLetters.Remove(availableLetters[randomNumber]);
            }

            return answerLetters;
        }
    }
}