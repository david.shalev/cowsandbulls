﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using Ex02.ConsoleUtils;

namespace bullsAndCows
{
    public static class userCommunicator
    {
        public static int getNumOfGuesses(int min, int max)
        {
            while (true)
            {
                Console.WriteLine("please type number of guesses between {0} - {1}", min, max);
                bool isNumber = Int32.TryParse(Console.ReadLine(), out int numOfGuesses);
                if (!isNumber)
                {
                    Console.WriteLine("the given input is not a number");
                    continue;
                }
                else if (numOfGuesses < 4 || numOfGuesses > 10)
                {
                    Console.WriteLine("the given input is not on the range");
                    continue;
                }
                return numOfGuesses;
            }
        }

        public static char[] getUserGuess(int numOfGuessLetter,List<char> legalLetters)
        {

            while (true)
            {
                Console.WriteLine("please type your next guess <A B C D> or Q for exit");
                string userSelect = Console.ReadLine();
                if (userSelect == "Q") Environment.Exit(0);
                if (!isGuessValid(userSelect,numOfGuessLetter,legalLetters))
                {
                    Console.WriteLine("the given input is not a valid guess");
                    continue;
                }
                return convertInputToGuess(userSelect,numOfGuessLetter);
            }
        }

        private static char[] convertInputToGuess(string userSelect,int numOfGuessLetters)
        {
            char[] guess= new char[numOfGuessLetters];
            for (int i = 0; i < userSelect.Length;i = i + 2)
            {
                guess[i / 2] = userSelect[i];
            }

            return guess;
        }

        private static bool isGuessValid(string userSelect,int numOfGuessLetters,List<char> legalLetters)
        {
            if (numOfGuessLetters != (userSelect.Length / 2) + 1)
            {
             
                return false;
            }

            List<char> currentUserGuess  =new List<char>();
            for (int i = 0; i < userSelect.Length; i++)
            {

                if (i % 2 == 0 && !legalLetters.Contains(userSelect[i])) return false;
                if (i % 2 != 0 && userSelect[i] != ' ') return false;
                if (i % 2 == 0 && currentUserGuess.Contains(userSelect[i])) return false;
                if (i % 2 == 0) currentUserGuess.Add(userSelect[i]);
            }

            return true;
        }

        public static bool isNewGame(bool isGameOver)
        {
            while (true)
            {
                if (isGameOver)
                {
                    Console.WriteLine("no more guesses allowed, you lost");
                }
                else Console.WriteLine("well done, you won!!!!!!!!!!!!");
                Console.WriteLine("would you like to start a new game ? <Y/N>");
                string userSelect = Console.ReadLine();
                if (userSelect != "Y" && userSelect != "N")
                {
                    Console.WriteLine("the only valid answers are <Y/N>");
                    continue;
                }
                if(userSelect =="N")Environment.Exit(0);
                Screen.Clear();
                return true;
            }
        }
    }
}
